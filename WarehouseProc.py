#import bpy #uncomment in blender
import random
import os
import re
import time
import json
import pandas as pd
#import pip
#pip.main(['install', 'numpy'])

class warehousebox():
    width = 1
    height = 1
    length = 1
    partsperV = 0
    colour = "Red"
    quantity = int(0)
    binhash = 'AA01A'
    binx = "AA"
    biny = 13
    binz = "G"
    partnumber = "1A0000000AA default"
    description = "Part desciption"
    originCountry = "not set"
    Supplier = "not set"
    Area = ''
    Continent = ''
    partcost = 0.0
    LHDRHD = 'not set'
    weightpart = 0.0
    weightbox = 0.0
    revision ='ZZ'
    valid = False
    def validcheck(self):
        if (self.width == 0) or (self.height == 0) or (self.length == 0):
            self.valid = False
        else:
            self.valid = True

    def getweightbox(self):
        self.weightbox = self.weightpart * self.quantity
        return self.weightbox
    

    def binzadjustment(self):
        try:
            self.binz = int(self.binz) + float(self.height/2)
        except:
            pass
            print("failed bin z adjustement")

    def Wadjustments(self):
        print(self.binhash)
        wtest = self.binhash[:1]
        if (wtest == "W") or (wtest == "w"):
            self.binz = self.binz*2
            #self.binx = sortstring("C" + self.binhash[1:2].upper()) - 44.15
            self.binx = sortstring(self.binhash[:2].upper())  + sortxlocadjust(dict2[self.binhash[1:2].upper()]) # - 1345
            self.biny = sortyloc(int(self.binhash[2:4])*2)
            # self.binhash = self.binhash.upper()
            print("w adjusted")


    def bins(self, bincode):
        #print(bincode)
        if re.match(r"[A-Z]+[A-Z]\d\d[A-Z]", bincode):
            self.binx = sortstring(bincode[:2])
            self.biny = sortyloc(int(bincode[2:4]))
            self.binz = sortz(dict2[bincode[4:6]]) 
            #print(self.binx, self.biny, self.binz)
            self.valid = True
        else:
            print("not valid " + bincode)
    
    def haslocation(self):
            if(self.binx == "AA") and (self.biny == 13) and (self.binz == "G"):
                return False
            else:
                self.valid = True
                return True

def setbins(bincode, height):
    #print(bincode)
    if re.match(r"[A-Z]+[A-Z]\d\d[A-Z]", bincode):
        binx = sortstring(bincode[:2])
        biny = sortyloc(int(bincode[2:4]))
        binz = sortz(dict2[bincode[4:6]]) 
        #print(self.binx, self.biny, self.binz)

        wtest = bincode[:1]
        if (wtest == "W") or (wtest == "w"):
            binz = binz*2
            binx = sortstring(bincode[:2].upper()) + sortxlocadjust(dict2[bincode[1:2].upper()]) #- 1345 
            biny = sortyloc(int(bincode[2:4])*2)
        binz = int(binz) + float(height/2)
        return(binx, biny, binz)
    else:
        return(0,20,0)


def sortstring(string):
    x= string[:1]
    y= string[1:2]
    test = dict1[x+y]
    
    length2 = 1.15
    length1 = 3.85
    location = 0.0
    
    while test>0:
            mod = test % 2 #if even
            if mod > 0:
                location = location + length1
            else:
                location = location + length2
            
            test = test-1
    return location

def sortyloc(intYloc):
    length1 = 1.3
    length2 = 1.49
    location = 0
    test = intYloc
    while test>1:
            mod = test % 2 #if even
            if mod > 0:
                location = location + length1
            else:
                location = location + length2
            
            test = test-1
    return location

def sortxlocadjust(intxloc):
    length1 = 0
    length2 = 1
    location = 0
    test = intxloc
    while test>1:
            mod = test % 2 #if even
            if mod > 0:
                location = location + length1
            else:
                location = location + length2
            
            test = test-1
    return location


def sortz(intzloc):
    length1 = 1.65 #first rack height
    length2 = 1.5 #rest of the rack heights
    location = 0
    test = intzloc

    while test>2:
            #mod = test % 2 #if even
            #if mod > 0:
            #    location = location + length1
            #else:
            location = location + length2
            
            test = test-1
    while test>1:
        location = location + length1
        test = test-1
    return location

def addjson(listofboxes, ListBOM=[], ListAnalytics=[]): # add box details to a json file
    counter=0
    print("Preparing to create .json data...")
    for box in listofboxes:
        newjson = {
            "id" : counter,
            "width" : box.width,
            "height" : box.height,
            "length" : box.length,
            "binx" : box.binx,
            "biny" : box.biny,
            "binz": box.binz,
            "valid" : box.valid,
            "binhash" : box.binhash,

            }

        metadatajson = {
            "id": counter,
            "binhash" : box.binhash,
            "description": box.description,
            "currentQuantity": box.quantity,
            "part": box.partnumber,
            "revision": box.revision,
            "supplierName": box.Supplier,
            #"originCountry": box.originCountry,
            "subjectArea": box.Area,
            #"minOrderSize": 100,
            "continent" : box.Continent, 
            #"partCost" : 0.0,
            #"LHDRHD" : 'not set',
            "partWeight" : box.weightpart,
            #"boxWeight" : box.weightbox,
        }


        #DICT[newjson]= "box"
        counter = counter + 1
        #print("did a box")
        LISTJSON.append(newjson)
        LISTMETAJSON.append(metadatajson)
        #listnotjson.append(jsonString)


    structure = {"boxes": LISTJSON}
    metadata = {"boxes": LISTMETAJSON}
    BOM = {"boxes":ListBOM}
    Analytics = {"analytics":ListAnalytics}
    #print(ListBOM)
    #print("hmmm")
    #print(LISTMETAJSON)

    root = {"structure": structure,
            "metadata": metadata,
            "BOM": BOM,
            "analytics": Analytics,}
    #filesavedto = 'WarehouseRecords3D.json' 
    print("Created json data, Saving to file...")

    return(root)

def filesaveJSON(root,filesavedto):
    import json
    import os
    with open(filesavedto,'w') as file:
         json.dump(root, file, sort_keys=True, indent=4)

    print("Complete. File saved to ", os.getcwd(), "\\", filesavedto)

# #################################################################

#creating dictionaries to turn AA, AB, AC to 1, 2, 3.
from string import ascii_uppercase
dict1 = dict2 = {}
a = b = 0
for i in ascii_uppercase:
    dict2[i] = b
    b+=1

cols = ["AA", "AB", "AC", "AD", "AE", "AF", "AG", "AH", "AJ", "AK", "AL", "AM",
            "AN", "AP", "AQ", "AR", "AS", "AT", "AU", "AV", "AW", "AX", "AY", "AZ",
            "BA", "BB", "BC", "BD", "BE", "BF", "BG", "BH", "BJ",
            "WA", "WB", "WC", "WD", "WE", "WF", "WG", "WH", "WJ", "WK"]

dict1 = { cols[i] : i for i in range(0, len(cols) ) }
LISTJSON = []
LISTMETAJSON=[]
DICT = {}


"""
Creating a Warehouse definition in JSON.
Target Output:

{
    "warehouse" : {
        "binLocations" : [
            {
                "binhash" : "AA01A",
                binx = "AA"
                biny = 01
                binz = "A"
                "X" : 0,
                "Y" : 0,
                "Z" : 0,
                "length" : 1,
                "width" : 1,
                "height" : 1,
                "volume" : 1,
            }
        ]
    }
}


"""


import WarehouseProc
import re

#1s are for A to B section. 2s are for W section. 3s will be for extra section.
rows1 = ["AA", "AB", "AC", "AD", "AE", "AF", "AG", "AH", "AJ", "AK", "AL", "AM",
            "AN", "AP", "AQ", "AR", "AS", "AT", "AU", "AV", "AW", "AX", "AY", "AZ",
            "BA", "BB", "BC", "BD", "BE", "BF", "BG", "BH", "BJ",] #XXXXXXXXXXXXX
dictX1 = { rows1[i] : i for i in range(0, len(rows1) ) }
lengths1 = ["01","02","03","04","05","06","07","08","09","10","11","12"] #YYYYYYYYYYYYY
dictY1 = { lengths1[i] : i for i in range(0, len(lengths1) ) }
heights1 = ["A", "B", "C", "D", "E", "F"] #ZZZZZZZZZZZZZZZZZZZ
dictZ1 = { heights1[i] : i for i in range(0, len(heights1) ) }

#W Section
rows2 = ["WA", "WB", "WC", "WD", "WE", "WF", "WG", "WH", "WJ", "WK"] #XXXXXX
dictX2 = { rows2[i] : i for i in range(0, len(rows2) ) }
lengths2 = ["01","02","03","04","05","06",] #YYYYYY
dictY2 = { lengths2[i] : i for i in range(0, len(lengths2) ) }
heights2 = ["A", "B", "C", ] #ZZZZZZZZZZ
dictZ2 = { heights2[i] : i for i in range(0, len(heights2) ) }

# C section
#rows3 = ["CA","CB","CC","CD","CE","CF","CG","CH","CI","CJ","CK","CL","CM","CN","CO","CP"] #XXXXXX
rows3 = ["CA","CB","CC","CD","CE",
         "CF","CG","CH","CI","CJ",
         "CK","CL","CM","CN","CO",]
rows4 = ["CP","CQ","CR","CS","CT","CU","CV",
         "CW","CX","CY","CZ","DA","DB","DC",
         "DD","DE","DF","DG","DH","DI","DJ",]

        #  "DK","DL","DM","DN",
        #  "DO","DP","DQ","DR","DS","DT","DU","DV","DW","DX",]
         #"DY","DZ"]
dictX3 = { rows3[i] : i for i in range(0, len(rows3) ) }
dictX4 = { rows4[i] : i for i in range(0, len(rows4) ) }
lengths3 = ["01"] #YYYYYY
dictY3 = { lengths3[i] : i for i in range(0, len(lengths3) ) }
# A B C
heights3 = ["A", "B", "C", "D", "E"] #ZZZZZZZZZZ
dictZ3 = { heights3[i] : i for i in range(0, len(heights3) ) } #ZZZZZZ


x=0
y=0
z=0
binhash = ""


def setbins(binhash):
    binx = (binhash[:2])
    biny = (binhash[2:4])
    binz = (binhash[4:6]) 
    return(binx, biny, binz)

def sortXaxis(numberofbinx):
    length2 = 1.15
    length1 = 3.85
    location = 0.0
    while numberofbinx>0:
            mod = numberofbinx % 2 #if even
            if mod > 0:
                location = location + length1
            else:
                location = location + length2 
            numberofbinx = numberofbinx-1
    return location

def sortXaxisW(numberofbinx):
    length1 = 0
    length2 = 1
    location = 0
    while numberofbinx>0:
            mod = numberofbinx % 2 #if even
            if mod > 0:
                location = location + length1
            else:
                location = location + length2
            
            numberofbinx = numberofbinx-1
    return location

def sortCXaxis(numberofbinx, length=3,):
    length2 = length
    length1 = length
    location = 0.0
    while numberofbinx>0:
            mod = numberofbinx % 2 #if even
            if mod > 0:
                location = location + length1
            else:
                location = location + length2 
            numberofbinx = numberofbinx-1
    return location

def sortYaxis(numberofbiny):
    length1 = 1.3
    length2 = 1.5
    location = 0
    while numberofbiny>0:
            mod = numberofbiny % 2 #if even
            if mod > 0:
                location = location + length1
            else:
                location = location + length2
            numberofbiny = numberofbiny-1
    return location

def sortZaxis(numberofbinz):
    length1 = 1.65 #first rack height
    length2 = 1.5 #rest of the rack heights
    location = 0
    while numberofbinz>1:
            location = location + length2
            numberofbinz = numberofbinz-1
    while numberofbinz>0:
        location = location + length1
        numberofbinz = numberofbinz-1
    #print(location)
    return location

def sortWZaxis(numberofbinz):
    length1 = 3 #first rack height
    location = 0
    while numberofbinz>0:
        location = location + length1
        numberofbinz = numberofbinz-1#
    
    return location

def sortCZaxis(numberofbinz):
    length1 = 1.5 #first rack height
    length2 = 1.5 #rest of the rack heights
    location = 0
    while numberofbinz>1:
            location = location + length2
            numberofbinz = numberofbinz-1
    while numberofbinz>0:
        location = location + length1
        numberofbinz = numberofbinz-1
    return location

def getAARacks(binLocations, width = 1, width2 = 1):
    #Create A to B racks
    length = 1.30
    #width = 1
    height = 1.4
    #1.2, 1, 1.37  #### 
    altwidths = True
    for i in dictX1: # AAAAAAAAAAAAAAAAAA
        altwidths =  not altwidths
        for j in dictY1:
            for k in dictZ1:
                binhash = i + j + k
                binx = sortXaxis(dictX1[i])
                biny = sortYaxis(dictY1[j])
                binz = sortZaxis(dictZ1[k]) + height/2

                if (altwidths):
                    widthfinal = width
                else:
                    widthfinal = width2

                warehouse1 = {"binhash" : binhash,
                    "binx" : i,
                    "biny" : j,
                    "binz" : k,
                    "X" : binx,
                    "Y" : biny,
                    "Z" : binz,
                    "length" : length,
                    "width" : widthfinal,
                    "height" : height,
                    "volume" : length*widthfinal*height,
                }
                binLocations.append(warehouse1)

def getWARacks(binLocations, width = 1, width2 = 1):
    #Create WA racks
    length = 2.5
    height = 2.6
    altwidths = False
    for i in dictX2: #WWWWWWWWWWWWWWWWW
        altwidths = not altwidths
        for j in dictY2:
            for k in dictZ2:
                binhash = i + j + k
                binx = sortXaxis(dictX2[i]+len(dictX1)) + sortXaxisW(dictX2[i])
                biny = sortYaxis(dictY2[j]*2 ) + 0.625
                binz = sortWZaxis(dictZ2[k]) + height/2

                if (altwidths):
                    widthfinal = width
                else:
                    widthfinal = width2


                warehouse1 = {"binhash" : binhash,
                    "binx" : i,
                    "biny" : j,
                    "binz" : k,
                    "X" : binx,
                    "Y" : biny,
                    "Z" : binz,
                    "length" : length,
                    "width" : widthfinal,
                    "height" : height,
                    "volume" : length*widthfinal*height,
                }
                binLocations.append(warehouse1)

def getCCRacks(binLocations,):
    #Create C racks1
    length = 1.8
    width = 3
    height = 1.4
    #1.2, 1, 1.37  #### 
    for i in dictX3: # AAAAAAAAAAAAAAAAAA
        for j in dictY3:
            for k in dictZ3:
                binhash = i + j + k
                val = int((dictX3[i])/5)
                binx = sortCXaxis(dictX3[i], length=width,) + (val)
                biny = 20
                binz = sortCZaxis(dictZ3[k]) + height/2

                warehouse1 = {"binhash" : binhash,
                    "binx" : i,
                    "biny" : j,
                    "binz" : k,
                    "X" : binx,
                    "Y" : biny,
                    "Z" : binz,
                    "length" : length,
                    "width" : width,
                    "height" : height,
                    "volume" : length*width*height,
                }
                binLocations.append(warehouse1)

def getCCRacks2(binLocations,):
    #Create C racks1
    length = 1.6
    width = 2
    height = 1.4
    #1.2, 1, 1.37  #### 
    for i in dictX4: # AAAAAAAAAAAAAAAAAA
        for j in dictY3:
            for k in dictZ3:
                binhash = i + j + k
                val = int((dictX4[i])/7)
                binx = sortCXaxis(dictX4[i], length=width,) + 60 + (val)
                biny = 20
                binz = sortCZaxis(dictZ3[k]) + height/2

                warehouse1 = {"binhash" : binhash,
                    "binx" : i,
                    "biny" : j,
                    "binz" : k,
                    "X" : binx,
                    "Y" : biny,
                    "Z" : binz,
                    "length" : length,
                    "width" : width,
                    "height" : height,
                    "volume" : length*width*height,
                }
                binLocations.append(warehouse1)

WAREHOUSENULL = {"binhash" : "ZZ01A",
                "binx" : "ZZ",
                "biny" : "01",
                "binz" : "A",
                "X" : 0,
                "Y" : -10,
                "Z" : 0,
                "length" : 10,
                "width" : 10,
                "height" : 10,
                "volume" : 10*10*10,
            }


def CreateBins():
    binLocations = []
    #getDumpLocation(binLocations)
    getAARacks(binLocations)
    getWARacks(binLocations)

    return binLocations

def CreateBins14():
    binLocations = []
    #getDumpLocation(binLocations)
    getAARacks(binLocations,width = 1.4,width2 = 1.4)
    getWARacks(binLocations, width = 1.4, width2 = 1.4)

    return binLocations

def CreateBinsSingle14():
    binLocations = []
    #getDumpLocation(binLocations)
    getAARacks(binLocations,width = 1.4,width2 = 1)
    getWARacks(binLocations, width = 1.4, width2 = 1)

    return binLocations

def CreateBinsCC():
    binLocations = []
    #getDumpLocation(binLocations)
    getAARacks(binLocations)
    getWARacks(binLocations)
    getCCRacks(binLocations)   
    getCCRacks2(binLocations)
    return binLocations

def CreateBinsALL():
    binLocations = []
    #getDumpLocation(binLocations)
    getAARacks(binLocations,width = 1.4,width2 = 1.4)
    getWARacks(binLocations, width = 1.4, width2 = 1.4)
    getCCRacks(binLocations)   
    getCCRacks2(binLocations)

    return binLocations

def savewarehouseonly(binLocations, filename1 = 'WarehouseDefinitionTest.json'):
    #warehouse = {"binLocations" : binLocations}
    structure2 = {"binLocations" : binLocations,
                    "boxes": []}

    root = WarehouseProc.addjson([], []) # creating base structure correctly
    #structure = root["structure"]
    metadata = root["metadata"]
    BOM =root["BOM"]
    analytics = root["analytics"]

    root = {"structure": structure2, # redefining it blank
            "metadata": metadata,
            "analytics": analytics,
            "BOM": BOM}



    print("saving...")
    #filename1 = 'WarehouseDefinitionTest.json'
    WarehouseProc.filesaveJSON(root,filename1)
    print("Saved file ", filename1 )



if __name__ == "__main__":
    #binLocations = CreateBins()
    binLocations = CreateBinsSingle14()
    #binLocations = CreateBins14()
    #binLocations = CreateBinsCC()
    
    filename1 = 'WarehouseForCC.json'

    savewarehouseonly(binLocations,filename1)
# import numpy as np 
# import pandas as pd 
# import matplotlib.pyplot as plt




# a = df[['PT0020']]
# a = a.apply(pd.to_numeric)
# a = a.dropna()
# hist, bin_edges = np.histogram(a, bins=20, range=None, weights=None, density=None)# 

# width = 0.95 * (bin_edges[1] - bin_edges[0])
# plt.bar(bin_edges[:-1], hist, width = width)
# plt.xlim(min(bin_edges), max(bin_edges))
# plt.show()
# print("done")

def histogramdef(df, rowname, binsnum, b_show= False, b_save = False, saveloc ='defaultgraph.png'):
    import numpy as np 
    import pandas as pd 
    import matplotlib.pyplot as plt

    a = df[[rowname]]
    a = a.apply(pd.to_numeric)
    a = a.dropna()
    print('a mean minutes: ')
    meanseconds = a.mean()
    meanmins = meanseconds/60
    print(meanmins)

    hist, bin_edges = np.histogram(a, bins=binsnum, range=None, weights=None, density=None)# 

    width = 0.95 * (bin_edges[1] - bin_edges[0])
    plt.bar(bin_edges[:-1], hist, width = width)
    plt.xlim(min(bin_edges), max(bin_edges))

    if b_show ==True:
        plt.show()
    if b_save ==True:
        plt.savefig(saveloc)
    print("plotted" + rowname)
    return(meanmins)



#y = plt.plot(bin_edges[:-1], hist,)

def histogramline(df, rowname, binsnum, b_show= False, b_save = False, saveloc ='defaultgraph.png'):
    import numpy as np 
    import pandas as pd 
    import matplotlib.pyplot as plt

    a = df[[rowname]]
    a = a.apply(pd.to_numeric)
    a = a.dropna()
    print('a mean minutes: ')
    meanseconds = a.mean()
    meanmins = meanseconds/60
    print(meanmins)

    lower = a[rowname].quantile(0.25)/60
    upper = a[rowname].quantile(0.75)/60
    # std = a.loc[:,rowname].std()
    # lower = (meanseconds - std)/60
    # upper = (meanseconds + std)/60
    # print('upper:', upper, ' lower: ', lower)

    hist, bin_edges = np.histogram(a, bins=binsnum, range=None, weights=None, density=None)# 

    #width = 0.95 * (bin_edges[1] - bin_edges[0])
    plt.plot(bin_edges[:-1], hist,)    
    plt.xlim(min(bin_edges), max(bin_edges))

    if b_show ==True:
        plt.show()
    if b_save ==True:
        plt.savefig(saveloc)
    print("plotted")
    return(meanmins, lower, upper)


# excel_file = 'IndexState Daily Processed as numbers.xlsx'
# df = pd.read_excel(excel_file)
# x = histogramline(df, 'PT0020', 20, b_save=True, saveloc= 'PT0020.png')

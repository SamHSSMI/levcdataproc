import histogram
import numpy as np 
import pandas as pd 
import matplotlib.pyplot as plt


excel_file = 'IndexState Daily Processed as numbers.xlsx'
df = pd.read_excel(excel_file)

machinelisttotal = ['PT0010', 'PT0020', 'PT0030', 'PT0040','PT0045',#'PT0050', 
'TR0060', 'TR0070', 'TR0090', 'TR0100',
'TR0120','TR0130', 'TR0140', 'TR0150', 'TR0160', 'TR0170', 'CH0200', 'CH0210',
'CH0220', 'CH0230', 'CH0240', 'CH0250', 'FN0260', 'FN0270', 'FN0280',
'FN0290', 'FN0300', 'FN0310', 'FN0320', 'HC0010', 'HC0020', 'HC0030',
'HC0040', 'HC0050', 'HC0060', 'HC0080', 'HC0090', 'HC0170', #'CC0010',
'CC0020', 'CC0030', 'CC0050', 'CC0060', 'CC0080', 'CC0090', 'CC0100',#'CC0140', 
'CC0160']

machinelistordered = [
'HC0010', 'HC0020', 'HC0030', 'HC0040', 'HC0050', 'HC0060', 'HC0080', 'HC0090', 'HC0170',
'CC0020', 'CC0030', 'CC0050', 'CC0060', 'CC0080', 'CC0090', 'CC0100', 'CC0160',
'PT0010', 'PT0020', 'PT0030', 'PT0040','PT0045',
'TR0060', 'TR0070', 'TR0090', 'TR0100','TR0120','TR0130', 'TR0140', 'TR0150', 'TR0160', 'TR0170',
'CH0200', 'CH0210', 'CH0220', 'CH0230', 'CH0240', 'CH0250', 
'FN0260', 'FN0270', 'FN0280','FN0290', 'FN0300', 'FN0310', 'FN0320',
]

machinelistPT = ['PT0010', 'PT0020', 'PT0030', 'PT0040','PT0045', #'PT0050',
]
machinelistTR = ['TR0060', 'TR0070', 'TR0090', 'TR0100',
'TR0120','TR0130', 'TR0140', 'TR0150', 'TR0160', 'TR0170',]
machinelistHotcoldCures = ['HC0010', 'HC0020', 'HC0030',
'HC0040', 'HC0050', 'HC0060', 'HC0080', 'HC0090', 'HC0170', #'CC0010',
'CC0020', 'CC0030', 'CC0050', 'CC0060', 'CC0080', 'CC0090', 'CC0100',#'CC0140',
 'CC0160']
machinelistFN = ['CH0200', 'CH0210',
'CH0220', 'CH0230', 'CH0240', 'CH0250', 'FN0260', 'FN0270', 'FN0280',
'FN0290', 'FN0300', 'FN0310', 'FN0320',]




means = []
lowers = []
uppers = []

for machine in machinelistordered:
    histogram.histogramdef(df, machine, 20, b_save=False, saveloc= machine + '.png')
    meanmins, upper, lower = histogram.histogramline(df, machine, 20, b_save=True, saveloc= machine + ' line.png')
    means.append(meanmins)
    lowers.append(lower)
    uppers.append(upper)
    #plt.savefig("Combined.png")
    plt.close()
    plt.xlim(0, 3500)
    
#x = histogram.histogramdef(df, 'PT0020', 20, b_save=True, saveloc= 'PT0020.png')
plt.xlim(0, 3500)
#plt.savefig("Combined.png")
print('done')


def plotzone(title, listofmachs):

    plt.close()
    plt.title(title + " Machine cycletimes.")
    plt.xlabel("Machines")
    plt.xticks( ticks = range(0, len(listofmachs)),labels = listofmachs, rotation=-90)
    plt.ylabel("Cycle Times (Minutes)")
    plt.plot(means)
    plt.plot(uppers)
    plt.plot(lowers)
    plt.show()

plotzone("All", machinelistordered)
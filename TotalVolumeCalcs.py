"""
Loading from Excel into json.

"""

def exceltojson(excel_file = 'Filtered Data - LEVC warehouse Master 3.xlsx'):
    import random
    import os
    import re
    import time
    import json
    import pandas as pd
    import WarehouseProc
    import numpy as np

    print(os.getcwd())
    os.chdir("C:/Users/Sam/Documents/LEVC")

    #Load data
    #excel_file = 'Filtered Data - LEVC warehouse Master 3.xlsx'
    #excel_file = 'Packaging data tracker corrected.xlsx'
    df = pd.read_excel(excel_file, sheet_name = "default")
    df = df.fillna("NaN")
    print(df.dtypes)


    listofboxes = []

    ListAA = ['AA','AB', 'AC','AA','AB','AC','AD','AE','AF','AG','AH',
    'AJ','AK','AL','AM','AN','AP','AP','AR','AS','AT','AU','AV','AX',
    'AY','AZ','BA','BB','BC','BD','BE','BF','BG','BH','BJ','WA','WB',
    'WC','WD','WE','WF','WG','WH','WJ','WK',]
    List12 = ["00","01","02","03","04","05","06","07","08","09","10","11","12"]   #np.arange(12)
    ListAF = ["A", "B", "C", "D", "E","F"]
    Placesref= []
    listBOM = []
    for AA in ListAA:
        for BB in List12:
            for CC in ListAF:
                Code = AA + BB + CC
                Placesref.append(Code)
                #print(Code)

    z=0
    xlineofparts = 0
    ylineofparts = 0
    lasthalfwidth = 0
    for x in range(0,  len(df)): #100): 1557): 

        for y in range (0, int(df.loc[x,"Number Of Boxes"])):
            newbox = WarehouseProc.warehousebox()
            newbox.partnumber = df.loc[x,"Part"]
            newbox.description = df.loc[x,"Description"]
            newbox.quantity = int(df.loc[x,"Parts Per Container"]) #changed from on hand

            newbox.length = float(df.loc[x,"Length"])/1000
            newbox.width = float(df.loc[x,"Width"])/1000
            newbox.height = float(df.loc[x,"Height"])/1000
            newbox.binhash = "AA14A" #Placesref[z]
            #binx, biny, binz = WarehouseProc.setbins(newbox.binhash, newbox.height)
            #newbox.bins(newbox.binhash)
            #newbox.binx = binx
            #newbox.biny = biny
            #newbox.binz = binz

            xlineofparts = xlineofparts + newbox.width/2 + lasthalfwidth
            newbox.binx = 0 + xlineofparts + 0.1
            newbox.biny = -10 + ylineofparts
            newbox.binz = newbox.height/2
            lasthalfwidth = newbox.width/2
            if xlineofparts>100:
                ylineofparts = ylineofparts - 3
                xlineofparts = 0

            newbox.revision = df.loc[x,"Revision"] #changed
            newbox.originCountry = df.loc[x,"Region"] # changed
            #newbox.weightpart = df.loc[x,"Total Weight"]
            newbox.Supplier = df.loc[x,"Supplier"]
            newbox.Area = df.loc[x,"Bin"]
            newbox.LHDRHD = df.loc[x,"Hand"]
            #print(z)
            z= z + 1
            if z> len(Placesref)-1:
                z=0


        
            if newbox.haslocation:
                newbox.validcheck()
                #newbox.binzadjustment()
                #newbox.Wadjustments()
                listofboxes.append(newbox)


            #print("got to BOM", df.columns)
            BOM ={
                "Part" : df.loc[x,"Part"],
                "Revision" : df.loc[x,"Revision"],
                "Description" : df.loc[x,"Description"],
                "Bin" : df.loc[x,"Bin"],
                "Supplier Name" : df.loc[x,"Supplier"],
                #"Box Type" : df.loc[x,"Box Type"],
                "Parts Per Container" : int(df.loc[x,"Parts Per Container"]),
                "No. of boxes per delivery" : int(df.loc[x,"No. of boxes per delivery"]),
                #"Pallet Qty" : int(df.loc[x,"Pallet Qty"]),
                "Iteration" : int(df.loc[x,"Iteration"]),
                "Length" : int(df.loc[x,"Length"]),
                "Width" : int(df.loc[x,"Width"]),
                "Height" : int(df.loc[x,"Height"]),
                #"Function" : df.loc[x,"Function"],
                "Supplier" : df.loc[x,"Supplier"],
                "Region" : df.loc[x,"Region"],
                #"Hand" : df.loc[x,"Hand"],
                #"RHD QTY" : float(df.loc[x,"RHD QTY"]),
                #"Station" : df.loc[x,"Station"],
                #"LHD QTY" : float(df.loc[x,"LHD QTY"]),
                "AV QTY" : float(df.loc[x,"AV QTY"]),
                #"Volume per Box" : float(df.loc[x,"Volume per Box"]),
                "Number Of Parts Required RHD" : int(df.loc[x,"Number Of Parts Required RHD"]),
                "Number Of Parts Required LHD" : int(df.loc[x,"Number Of Parts Required LHD"]),
                "Number Of Parts Required Total" : int(df.loc[x,"Number Of Parts Required Total"]),
                "Number Of Boxes" : int(df.loc[x,"Number Of Boxes"]),

            }
            listBOM.append(BOM)
            #print("got past BOM")
        #print("iteration: ", x)

    return listofboxes, listBOM


if __name__ == "__main__":
    import WarehouseProc
    listofboxes, listBOM = exceltojson(excel_file = 'Filtered Data - LEVC warehouse Master 3.xlsx')
    root = WarehouseProc.addjson(listofboxes, listBOM)
    filesavedto = 'WarehouseRecordsBOM.json'
    WarehouseProc.filesaveJSON(root,filesavedto)

    print(root)

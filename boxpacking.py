"""
This is a script to load .json data, process the locational data to add adjustments, then save it to a .json file again.
This is so the visualisation can be adjusted, so boxes aren't shown in the same location.  


"""
import json
import pandas as pd
import WarehouseProc  
import os
print('Begin box packing. Adjusting location data to avoid intersection of boxes in the same location. ')
newframe = pd.DataFrame({
    'binx': [],
    'biny': [], 
    'binz': [],
    'height': [],
    'id': [],
    'length': [],
    'width': [],
    "binhash" : [],
})
print("Opening .json...")


#with open('WarehouseRecords3D.json') as json_file:  
os.chdir("Z:/APC 11 REEV")
with open('WarehouseRecordsBOMBinhash2.json') as json_file:  
    root = json.load(json_file)

structure = root["structure"]
metadata = root["metadata"]
BOM =root["BOM"]
analytics = root["analytics"]
listjson = structure["boxes"]
metadatajson= metadata["boxes"]

WarehouseProc.warehousebox()
print("File loaded, processing data")
for recordbox in listjson:
    #print(recordbox["binhash"])
    newframe = newframe.append({
            'binx': recordbox['binx'],
            'biny': recordbox['biny'], 
            'binz': recordbox['binz'],
            'height': recordbox['height'],
            'id': recordbox['id'],
            'length': recordbox['length'],
            'width': recordbox['width'],
            "binhash" : recordbox['binhash'],
            "valid" : recordbox['valid']
    }, ignore_index=True)
print('loaded .json file back in...')

newframe = newframe[newframe.binx != "AA"]
newframe = newframe.fillna("NaN")

dups3 = newframe[newframe.duplicated(['binhash'], keep=False)]

for x in range(0, len(newframe["id"])):
    print(newframe.iloc[x]["binhash"])
    newframe.at[ int(x),'binx'], newframe.at[ int(x),'biny'], newframe.at[ int(x),'binz'] = WarehouseProc.setbins(newframe.iloc[x]["binhash"].upper(), float(newframe.iloc[x]["height"]))
    # = WarehouseProc.sortstring(newframe.iloc[x]["binhash"][:2])

binhash1 = ""
oldheight = 0
dups3 = dups3.sort_values(by=["binhash"], ascending=True)
dups3 = dups3[dups3.binx != "AA"]
print("altering locations if two or more parts are found in the same location.")
for x in range(0, len(dups3["id"])):
    #print(x)

    row = dups3.iloc[[int(x)]]
    #print(row)
    print("comparing ", row.iloc[0]["binhash"], binhash1)
    if row.iloc[0]["binhash"]==binhash1: #if the location is the same as the previous
        idpls = row.iloc[0]["id"]

        dups3.at[ int(idpls),'binz'] = float(dups3.at[ int(idpls),'binz']) + oldheight #add the height of the previous box
        oldheight = oldheight + row.iloc[0]["height"]
        if dups3.at[ int(idpls),'height']== 0:
            oldheight = oldheight + 1    
    else:
        binhash1 = row.iloc[0]["binhash"]
        oldheight = row.iloc[0]["height"] #set old height to box below

dfList = list(dups3['id'])
print("length of Duplicate/adjusted values: ", len(dfList))
#print(dfList)
print("before adjustment: ", len(newframe))
newframe = newframe[~newframe["id"].isin(dfList)]
print("after dropping values: ", len(newframe))
newframe = newframe.append(dups3)
print("final full: ", len(newframe))
print("Processed data, now saving back to file...")

counter=0
listjson2=[]
for x in range(0, len(newframe)):
    newjson = {
        "id" : newframe.iloc[x]["id"],
        "width" : newframe.iloc[x]["width"],
        "height" : newframe.iloc[x]["height"],
        "length" : newframe.iloc[x]["length"],
        
        "binhash" : newframe.iloc[x]["binhash"],
        "valid" : newframe.iloc[x]["valid"],
        "binx" : newframe.iloc[x]["binx"],
        "biny" : newframe.iloc[x]["biny"],
        "binz": newframe.iloc[x]["binz"],
        }


 
    listjson2.append(newjson)
    #     listmetajson.append(metadatajson)
    #     #listnotjson.append(jsonString)


structure2 = {"boxes": listjson2}
#metadata = {"boxes": listmetajson}
root = {"structure": structure2,
        "metadata": metadata,
        "analytics": analytics,
        "BOM": BOM}
print("length of adjusted frame: ", len(newframe))
print("List of json files length: ", len(listjson2))
print("saving...")
with open('WarehouseRecordsBOMBinhash3.json','w') as file:
         json.dump(root, file, sort_keys=True, indent=4)

print("maybe worked")

print("Completed boxpacking")


















# if (binWidth is smaller than binHeight and binDepth) then
# {
#     packByWidth = true
#     packByHeight = false
# }
# else if (binDepth is smaller than binHeight and binWidth) then
# {
#     packByWidth = false
#     packByHeight = false // both false
#     implies pack by depth
# }
# else if (binHeight is smaller than binWidth and binDepth) then

# {
#     packByWidth = false X
#     packByHeight = true
# }
# notPacked = Items

# {
#     toPack = notPacked
#     notPacked = {} // clear notPacked
#     Create a new bin called currentBin and check whether the item toPack[0]
#     is able to fit in this bin at position(x, y, z) = (0, 0, 0).
#     if toPack[0] does not fit then rotate it(over the six rotation types) until it fits and pack itinto this bin at postion(0, 0, 0).
#     for i = 1 to(size of toPack-1) do
#     {
#         currentItem = toPack[i]
#         fitted = false

#         for p = 0 to 2 do
#         {
#             k = 0
#             d while (k < number of items in currentBin) and (not fitted)
#             {
#                 binItem = kth item in currentBin
#                 if (packByWidth) then
#                     pivot = p
#                 else if (packByHeight) then
#                     switch(p)
#                 {
#                     compute pivot p for height
#                 }
#                 else // pack by depth
#                     switch(p)
#                 {
#                     compute pivot p for depth
#                 }
#                 switch(pivot)
#                 {
#                     case 0: Choose(pivotX, pivovY,
#                                    pivotZ) as the back lower
#                     right r i
#                     break
#                     corne of b nItem
#                     case 1: Choose(pivotX, pivovY,
#                                    pivotZ) as the front lower
#                     3
#                     left corner of binItem
#                     break
#                     case 2: Choose(pivotX, pivovY,
#                                    pivotZ) as the back Upper
#                     left corner of binItem
#                     break
#                 }
#                 if (currentItem can be packed
#                     in currentBin at
#                     position(pivotX, pivotY, pivotZ)) then
#                 {
#                     Pack currentItem into
#                     currentBin at position
#                     (pivotX, pivotY, pivotZ).
#                     fitted = true
#                 }
#                 else
#  { // try rotating item
#                     do
#                     Rotate currenItem
#                     while (currentItem cannot be
#                            packed in currentBin at

#                            position(pivotX, pivotY))
#                     and (not all
#                          rotations for currentItem
#                          checked)
#                     if (currentItem can be packed
#                         in currentBin at
#                         position(pivotX, pivotY,
#                                  pivotZ)) then
#                     {
#                         Pack currentItem into
#                         currentBin at position
#                         (pivotX, pivotY, pivotZ).
#                         fitted = true
#                     }else
#                     Restore currentItem to
#                     its original rotation type
#   }

#                 if (not fitted) then
#                 Add currentItem to the list
#                 notPacked
#             }
#         }
#     }
# } while notPacked has at least one
# Item in it

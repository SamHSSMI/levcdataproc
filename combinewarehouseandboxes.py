"""
Takes in a warehouse file and matches it with a defined warehouse.

"""

import DefineWarehouse
import WarehouseProc
import sys
import json



def loadrootfromstr():
    workflowIn = ""
    c = sys.stdin.read(1) # read in other data

    while c is not "\x04":
        workflowIn += c
        c = sys.stdin.read(1)

    sys.stdout.write(workflowIn + " from python.")

    root = json.loads(workflowIn)
    return root


def getrootandwarehouse(root,binLocations):

    structure = root["structure"]
    boxes = structure["boxes"]
    structure2 = {"binLocations" : binLocations,
                    "boxes": boxes}
    metadata = root["metadata"]
    BOM =root["BOM"]
    analytics = root["analytics"]

    root = {"structure": structure2, # redefining it blank
            "metadata": metadata,
            "analytics": analytics,
            "BOM": BOM}

    return(root)

    # print("saving...")
    # filename1 = 'WarehouseDefinitionTest2.json'
    # WarehouseProc.filesaveJSON(root,filename1)
    # print("Saved file ", filename1 )
    # print(root)

if __name__ == "__main__":
    binLocations = DefineWarehouse.CreateBins() # get warehouse 1
    loadrootfromstr()
    #root = getrootandwarehouse(root,binLocations)
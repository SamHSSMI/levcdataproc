import random
import os
import re
import time
import json
import pandas as pd
import WarehouseProc
import math
#import pip
#pip.main(['install', 'numpy'])


#################################################################
def loadcurrentstock(excel_file = 'Filtered Data - LEVC warehouse Master.xlsx'):
    print("begin Initial data processing")

    #set OS location
    #print(os.getcwd())
    #os.chdir("C:/Users/Sam/Documents/LEVC")
    #Load data
    #excel_file = 'Filtered Data - LEVC warehouse Master.xlsx'
    #excel_file = 'Packaging data tracker corrected.xlsx'
    df = pd.read_excel(excel_file, ) #sheet_name = "warehouse dataset")
    df = df.fillna("NaN")
    listofboxes = []
    xlineofparts = 0
    ylineofparts = 0
    lasthalfwidth = 0

    #get data on boxes from excel, save into list of classes
    import DefineWarehouse
    binLocations = DefineWarehouse.CreateBins()
    for x in range(0,  len(df)): #100): 1557): 
        newbox = WarehouseProc.warehousebox()

        newbox.partnumber = df.loc[x,"Part"]
        newbox.description = df.loc[x,"Description"]
        newbox.quantity = int(df.loc[x,"On Hand"])

        lengthyboi = float(df.loc[x,"Length"])/1000
        if math.isnan(lengthyboi):
            lengthyboi = 0.0
        newbox.length = lengthyboi
        newbox.width = float(df.loc[x,"Width"])/1000
        newbox.height = float(df.loc[x,"Height"])/1000
        newbox.binhash = df.loc[x,"Bin"].upper()
        #newbox.bins(df.loc[x,"Bin"])
        #print("code: ", df.loc[x,"Bin"].upper())
        #binx, biny, binz = WarehouseProc.setbins(df.loc[x,"Bin"].upper(), float(df.loc[x,"Height"])/1000)

        ###############
        binlocationtoputpart = next((item for item in binLocations if item["binhash"] == newbox.binhash), DefineWarehouse.WAREHOUSENULL)
        #print(binlocationtoputpart["binx"])
        newbox.binx = binlocationtoputpart["X"]    
        newbox.biny = binlocationtoputpart["Y"]
        newbox.binz = binlocationtoputpart["Z"]

        if binlocationtoputpart == DefineWarehouse.WAREHOUSENULL:
            xlineofparts = xlineofparts + newbox.width/2 + lasthalfwidth
            newbox.binx = binlocationtoputpart["X"] + xlineofparts + 0.1
            newbox.biny = binlocationtoputpart["Y"] + ylineofparts
            newbox.binz = newbox.height/2
            lasthalfwidth = newbox.width/2
            if xlineofparts>100:
                ylineofparts = ylineofparts - 3
                xlineofparts = 0
            newbox.binhash = "ZZ01A"

        ##########


        newbox.revision = df.loc[x,"Revision to"]
        newbox.Continent = df.loc[x,"Region"]
        newbox.weightpart = df.loc[x,"Total Weight"]
        newbox.Supplier = df.loc[x,"Supplier"]
        newbox.Area = df.loc[x,"Subset Area"]

        
        if newbox.haslocation:
            newbox.validcheck()
            #newbox.binzadjustment()
            #newbox.Wadjustments()
            listofboxes.append(newbox)
        #print("iteration: ", x)

    return listofboxes
#saving into json

# def loadcurrentstocktojson(excel_file = 'Filtered Data - LEVC warehouse Master.xlsx'):
#     print("begin Initial data processing")

#     df = pd.read_excel(excel_file, ) #sheet_name = "warehouse dataset")
#     df = df.fillna("NaN")
#     listofboxes = []

#     #get data on boxes from excel, save into list of classes
#     binLocations = DefineWarehouse.CreateBins()
#     for x in range(0,  len(df)): #100): 1557): 
#         newbox = WarehouseProc.warehousebox()

#         newbox.partnumber = df.loc[x,"Part"]
#         newbox.description = df.loc[x,"Description"]
#         newbox.quantity = int(df.loc[x,"On Hand"])

#         lengthyboi = float(df.loc[x,"Length"])/1000
#         if math.isnan(lengthyboi):
#             lengthyboi = 0.0
#         newbox.length = lengthyboi
#         newbox.width = float(df.loc[x,"Width"])/1000
#         newbox.height = float(df.loc[x,"Height"])/1000
#         newbox.binhash = df.loc[x,"Bin"].upper()
#         #newbox.bins(df.loc[x,"Bin"])
#         #print("code: ", df.loc[x,"Bin"].upper())
#         #binx, biny, binz = WarehouseProc.setbins(df.loc[x,"Bin"].upper(), float(df.loc[x,"Height"])/1000)

#         ###############
#         import DefineWarehouse
#         binlocationtoputpart = next((item for item in binLocations if item["binhash"] == newbox.binhash), DefineWarehouse.WAREHOUSENULL)
#         #print(binlocationtoputpart["binx"])
#         newbox.binx = binlocationtoputpart["X"]
#         newbox.biny = binlocationtoputpart["Y"]
#         newbox.binz = binlocationtoputpart["Z"]

#         ##########


#         newbox.revision = df.loc[x,"Revision to"]
#         newbox.Continent = df.loc[x,"Region"]
#         newbox.weightpart = df.loc[x,"Total Weight"]
#         newbox.Supplier = df.loc[x,"Supplier"]
#         newbox.Area = df.loc[x,"Subset Area"]





if __name__ == "__main__":
    listofboxes = loadcurrentstock(excel_file = 'Filtered Data - LEVC warehouse Master.xlsx')
    root =WarehouseProc.addjson(listofboxes)


    filesavedto = 'WarehouseRecordsCurrent.json'




    print("Completed WarehouseProcRun")
    print(root)

    WarehouseProc.filesaveJSON(root,filesavedto)
#import boxpacking

#import TotalVolumeCalcs
#import Setxyz
#import boxpacking


import os
import WarehouseProc
import WarehouseProcRun
import combinewarehouseandboxes
import DefineWarehouse
import TotalVolumeCalcs

os.chdir("C:/Users/Sam/Documents/LEVC")

########## Generate Warehouse #########
binLocations = DefineWarehouse.CreateBins() # get warehouse 0
#binLocations = DefineWarehouse.CreateBinsSingle14() # get warehouse 1
#binLocations = DefineWarehouse.CreateBins14() # get warehouse 2
#binLocations = DefineWarehouse.CreateBinsCC() # get warehouse 3
#binLocations = DefineWarehouse.CreateBinsALL() # get warehouse 5


####### Current Stock #############
#listofboxes = WarehouseProcRun.loadcurrentstock(excel_file = 'LEVC warehouse Current Stock matchedBOM.xlsx')
#root = WarehouseProc.addjson(listofboxes)

#OR#
####### Predicted Stock ##############
listofboxes, listBOM = TotalVolumeCalcs.exceltojson(excel_file = 'Filtered Data - LEVC warehouse Master Assumed.xlsx') # load excel
#listofboxes, listBOM = TotalVolumeCalcs.exceltojson(excel_file = "Filtered Data - LEVC warehouse Master Mezz202#1.xlsx") # mezzanine
root = WarehouseProc.addjson(listofboxes) # no BOM




#root = WarehouseProc.addjson(listofboxes, listBOM) # structure correctly
######## Matching stock to warehouse ######
root = combinewarehouseandboxes.getrootandwarehouse(root,binLocations) # combine with warehouse 1

#os.chdir("Z:/APC 11 REEV/Scenarios/Current Stock Analysis/Control Base Warehouse")
os.chdir("Z:/APC 11 REEV/Scenarios/0 Control Base Warehouse/2022")
#os.chdir("Z:/APC 11 REEV/Scenarios/1 Single Rack Widening/2022")
#os.chdir("Z:/APC 11 REEV/Scenarios/2 Double Rack Widening/2022")
#os.chdir("Z:/APC 11 REEV/Scenarios/3 Extra C Rack/2022")
#os.chdir("Z:/APC 11 REEV/Scenarios/4 Mezzanine/2022")
#os.chdir("Z:/APC 11 REEV/Scenarios/5 Combined Effect/2022")



WarehouseProc.filesaveJSON(root,"2022VolumeBaseWarehouse.json") # save file
#WarehouseProc.filesaveJSON(root,"2022VolumeSingle14Warehouse.json") # save file
#WarehouseProc.filesaveJSON(root,"2022VolumeDouble14Warehouse.json") # save file
#WarehouseProc.filesaveJSON(root,"2022VolumeCCWarehouse.json") # save file
#WarehouseProc.filesaveJSON(root,"2022VolumeMezzanineWarehouse.json") # save file
#WarehouseProc.filesaveJSON(root,"2022VolumeALLWarehouse.json") # save file


print('COMPLETED BOTH SCRIPTS')
print('\007')
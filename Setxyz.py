"""
Old script, now redundant, as included in 
"""

import json
import pandas as pd
import WarehouseProc  
import os
print('Location Reconfig')
newframe = pd.DataFrame({
    'binx': [],
    'biny': [], 
    'binz': [],
    'height': [],
    'id': [],
    'length': [],
    'width': [],
    "binhash" : [],
})


print("Opening .json...")

os.chdir("Z:/APC 11 REEV")

#with open('WarehouseRecords3D.json') as json_file:  
with open('WarehouseRecordsBOMBinhash.json') as json_file:  
    root = json.load(json_file)

structure = root["structure"]
metadata = root["metadata"]
listjson = structure["boxes"]
#metadatajson= metadata["boxes"]
analytics = root["analytics"]
BOM =root["BOM"]
#volume = analytics["volume"]


WarehouseProc.warehousebox()
print("File loaded, processing data")
for recordbox in listjson:
    print(recordbox["binhash"])
    binx, biny, binz = WarehouseProc.setbins(recordbox['binhash'], recordbox['height'])

    newframe = newframe.append({
            'binx': binx,
            'biny': biny, 
            'binz': binz,
            'height': recordbox['height'],
            'id': recordbox['id'],
            'length': recordbox['length'],
            'width': recordbox['width'],
            "binhash" : recordbox['binhash'],
            "valid" : recordbox['valid'],
    }, ignore_index=True)
print('loaded structures part of .json file back in...')


# print('loaded metadata part of .json file back in...')
print('loaded .json file back in...')

# newframe = pd.merge(newframe, newframe2, left_on='id', right_on='id', how='left')
listofboxes=[]
print("got this many parts: ", len(newframe))

listjson2=[]

for x in range(0, len(newframe)):
    newjson = {
        "id" : newframe.iloc[x]["id"],
        "width" : newframe.iloc[x]["width"],
        "height" : newframe.iloc[x]["height"],
        "length" : newframe.iloc[x]["length"],
        
        "binhash" : newframe.iloc[x]["binhash"],
        "valid" : newframe.iloc[x]["valid"],
        "binx" : newframe.iloc[x]["binx"],
        "biny" : newframe.iloc[x]["biny"],
        "binz": newframe.iloc[x]["binz"],
        }

    listjson2.append(newjson)
    #     listmetajson.append(metadatajson)
    #     #listnotjson.append(jsonString)


structure2 = {"boxes": listjson2}
#metadata = {"boxes": listmetajson}
root = {"structure": structure2,
        "metadata": metadata,
        "analytics": analytics,
        "BOM": BOM}
print("length of adjusted frame: ", len(newframe))
print("List of json files length: ", len(listjson2))
print("saving...")
with open('WarehouseRecordsBOMBinhash2.json','w') as file:
         json.dump(root, file, sort_keys=True, indent=4)

print("maybe worked")

print("Completed Setxyz")
